import numpy as np
import itertools

###################################
# We put the puzzle in the middle of a larger frame, and compute the graph of all possible moves
###################################

def cut(x, S): return min(S-1, max(0, x))

# The directions
Directions = {
    'L': (-1, 0, 0),
    'R': (1, 0, 0),
    'F': (0, -1, 0),
    'B': (0, 1, 0),
    'U': (0, 0, 1),
    'D': (0, 0, -1)
}




##############################################
# The Board class
##############################################
infty = 99999


class Board: #creates a board with the position of the pieces
    def __init__(self, dict_pieces, pos_pieces = None):
        # Class to solve the board
        # postPieces is a dict of the form label --> offset = (Ox, Oy, Oz), which is the relative position of the piece in the board
        # We use a dictionnary of the form cell --> numPiece. The correspondence Piece --> list_cells is already in the Class Piece.

        self.dict_pieces = dict_pieces # the "alive" pieces
        
        # initialisation of posPieces
        if pos_pieces == None:
            pos_pieces = {}
            for i in dict_pieces:
                pos_pieces[i] = (0,0,0) #no offset initially
        self.pos_pieces = pos_pieces
        self.name = tuple ( pos_pieces.items() ) # a unique name to compare boards

        # creation of cells
        self.cells = {}
        for i, piece in dict_pieces.items():
            Ox, Oy, Oz = pos_pieces[i] # the offset
            for (x,y,z) in piece.list_cells:
                self.cells[(x+Ox, y+Oy, z+Oz)] = i

    
    def can_move(self, setPieces, direction): 
        # setPieces contains the label of pieces we want to move
        # we check if we can move the pieces in the wanted direction
        dx, dy, dz = Directions[direction]

        for i in setPieces:
            piece = self.dict_pieces[i]
            Ox, Oy, Oz = self.pos_pieces[i]
            for (x,y,z) in piece.list_cells:
                xx, yy, zz = x+Ox+dx, y+Oy+dy, z+Oz+dz
                if (xx,yy,zz) in self.cells and self.cells[(xx,yy,zz)] not in setPieces:
                    return False
        return True
    
    def can_escape(self, setPieces, direction):
        # setPieces contains the label of pieces we want to move
        # we check if we can move the pieces in the wanted direction
        dx, dy, dz = Directions[direction]

        for i in setPieces:
            piece = self.dict_pieces[i]
            Ox, Oy, Oz = self.pos_pieces[i]
            for n in range(10): #TODO, finer limit
                for (x,y,z) in piece.list_cells:
                    xx, yy, zz = x+Ox+n*dx, y+Oy+n*dy, z+Oz+n*dz
                    if (xx,yy,zz) in self.cells and self.cells[(xx,yy,zz)] not in setPieces:
                        return False
        return True

    def get_all_children(self, verbose = 0):
        # returns the list of all possible boards which are accessible from this board with a move or an escape
        # we record only the new possible position of the Pieces (pos_pieces)
        # Outputs a list whose elements are of the form (new_s, setPieces, direction, type), with type = 'escape' or 'move'.

        listChildren = [] #the list of solutions

        # we consider all sets of pieces
        setPieces = set( self.dict_pieces.keys() ) # all numbers present
        all_sets = sum( [[s for s in itertools.combinations(setPieces, n)] for n in range(1, len(setPieces)//2+1 )], [] )

        ## First we check if the pieces can escape. If it the case, we stop here
        for setPieces in all_sets:
            for direction in Directions:
                if self.can_escape(setPieces, direction):
                    if verbose: print("Pieces ", setPieces, " can escape in direction ", direction)

                    # create new pos_pieces
                    new_dict_pieces = self.dict_pieces.copy()
                    new_pos_pieces = self.pos_pieces.copy()

                    for p in setPieces:
                        new_dict_pieces.pop(p)
                        new_pos_pieces.pop(p)

                    new_board = Board(new_dict_pieces, new_pos_pieces)
                    move = (setPieces, direction, 'escape')
                    listChildren.append( (new_board, move) )
                    return listChildren # we stop directly here

        ## Now we check if some pieces can move.
        for setPieces in all_sets:
            for direction in Directions:
                if self.can_move(setPieces, direction):
                    if verbose: print("Pieces ", setPieces, " can move in direction ", direction)
        
                    # create new pos_pieces
                    new_pos_pieces = self.pos_pieces.copy()
                    dx, dy, dz = Directions[direction]

                    for p in setPieces:
                        Ox, Oy, Oz = self.pos_pieces[p]
                        new_pos_pieces[p] = (Ox+dx, Oy+dy, Oz+dz)
                
        
                    # we perform a global translation to keep the puzzle in the center
                    Xmin, Ymin, Zmin = infty, infty, infty
                    #get minimum values
                    for i,piece in self.dict_pieces.items() :
                        Ox, Oy, Oz = new_pos_pieces[i]
                        for (x, y, z) in piece.list_cells:
                            Xmin, Ymin, Zmin = min(Xmin, x+Ox), min(Ymin, y+Oy), min(Zmin, z+Oz)
                    # translate all
                    for i, (Ox, Oy, Oz) in new_pos_pieces.items() :
                        new_pos_pieces[i] = Ox-Xmin, Oy-Ymin, Oz-Zmin

                    # we add the new positions
                    new_board = Board(self.dict_pieces, new_pos_pieces)
                    move = (setPieces, direction, 'move')
                    listChildren.append( (new_board, move) )
        return listChildren


################################################
# We need to compute a graph. We introduce the class Child to run through the graph
################################################

class Child: #the child class for the search. This will be the elements in the dictionnary.
    def __init__(self, parent, board, move, height):
        self.parent = parent    # the name of the parent
        self.board = board      # the actual board
        self.height = height    # distance to the initial puzzle
        self.move = move # the last set of pieces moved
        self.name = board.name



class Solver:
    def __init__(self, puzzle):        
        # initialise the graph of states
        self.puzzle = puzzle
        self.graphStates = {} # of the form name --> Child
        self.toVisit = []
        self.liststates = None
        self.soltype = None

        # create first node
        first_board = Board(puzzle.dict_pieces)
        height = 0
        first_child = Child(parent = "", board = first_board, move = None,  height = height)

        name = first_child.name

        self.graphStates[name] = first_child
        self.toVisit.append( (name, height) )

        #self.name_end_board = None
    
    def solve(self, verbose = 0):
        Niter = 300 #max moves

        for _ in range(Niter):
            # return if the list is empty
            if len( self.toVisit) == 0 : break

            #else we first sort the list by decreasing height. So we can use pop()
            self.toVisit.sort(key = lambda val : -val[1])

            parent_name, parent_height = self.toVisit.pop()

            if verbose: print("\nVisiting s = ", parent_name)
            
            parent = self.graphStates[parent_name].board # the corresponding board.
            listChildren = parent.get_all_children(verbose = verbose)

            if verbose: print("new states = ", listChildren)

            for newboard, move in listChildren:
                name, height = newboard.name, parent_height+1

                if name not in self.graphStates: #not yet visited
                    # We add the new children
                    newChild = Child(parent_name, newboard, move, height)
                    self.graphStates[name] = newChild

                    # If there is only one piece left, we are done
                    if len(newboard.dict_pieces) == 1:
                        self.name_end_board = name
                        ans = self.get_solution()
                        return ans # we solved the board

                    # Else we will need to visit it
                    self.toVisit.append( (name, height) )
                    if move[2] == 'escape': #we can erase the list to visit, and start from here
                        self.toVisit = [(name, height)]
        # not solvable
        ans = {
            "solved": False,
            "list_boards": None,
            "list_moves": None,
            "soltype": None,
            "Nmoves": None
        }
        return ans

    def get_solution(self, pos = ""):
        assert self.name_end_board is not None, "There is no solution"

        list_boards = []
        list_moves = []

        name = self.name_end_board
        state = self.graphStates[name]
        h = state.height

        while h > 0:
            list_boards.append(state.board)
            list_moves.append(state.move)
            state = self.graphStates[ state.parent ]
            h = state.height


        #reverse the list
        list_boards.reverse()
        #list_moves.pop()
        list_moves.reverse()

        # solution score
        soltype = []
        n = 0 #number of moves before escape
        curPieces, curDir = [-1], "O"
        
        for move in list_moves:
            n += 1
            if move[2] == "escape":
                soltype.append(n)
                n = 0
            if move[0] == curPieces and move[1] == curDir: #same piece, same move
                n -= 1 # we do not count it, as in burrtools.
            curPieces, curDir = move[0], move[1]

        # total number of move
        Nmoves = sum(soltype)

        #create answer

        ans = {
            "solved": True,
            "list_boards": list_boards,
            "list_moves": list_moves,
            "soltype": soltype,
            "Nmoves": Nmoves
        }
        return ans
