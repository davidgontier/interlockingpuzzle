import multiprocessing
from multiprocessing import Pool
import numpy as np
import random

import Puzzle


####################################
# In the sequel, we assume that we are given global parameters (in parameters.py)
from parameters import *

fixed = fixed.replace(" ", "")
fixed = fixed.replace("\n", "")


#listPiecesOrHoleAdd = listPieces + ["0"] #if we want to add a hole
listPiecesOrHoleAdd = listPieces 

def change(s):
    # this function takes a random puzzle, and flips one bit randomly
    i = random.randint(0, len(s)-1)
    if fixed[i] == 'C': # We can add a piece or a hole
        c = listPiecesOrHoleAdd[random.randint(0, len(listPiecesOrHoleAdd) - 1)]
    else: # we cannot touch it
        c =  s[i]
    news = s[:i] + c + s[i+1:]
    return news


def randomSearch(myinput):
    # this code is run by a single processor
    # the entry is an initial puzzle, and the number of steps the processor can do
    s, Nflips = myinput

    s = s.replace(' ', '')
    s = s.replace('\n', '')

    # initialization
    puzzle = Puzzle.Puzzle(s, sizePuzzle)
    ans = puzzle.solve()

    if ans["solved"]:
        Nmoves = ans["Nmoves"]
        soltype = ans["soltype"]
    else:
        Nmoves = 0
        soltype = []


    # main loop
    for iflip in range(Nflips):
        news = change(s)
        if news == s: pass #no new puzzle

        puzzle = Puzzle.Puzzle(news, sizePuzzle)

        if puzzle.is_valid():
            ans = puzzle.solve()
            if ans["solved"]:
                # if as good as before, we accept the change
                if ans["Nmoves"] >= Nmoves or (ans["Nmoves"] == Nmoves and ans["soltype"] >= soltype):
                #if ans["soltype"] >= soltype:
                
                    # if strictly better, we print the new result
                    if ans["Nmoves"] > Nmoves or (ans["Nmoves"] == Nmoves and ans["soltype"] > soltype):
                    #if ans["soltype"] > soltype:
                        print("\niflip = ", iflip)
                        print("s = ", s)
                        print("solType = ", ans["soltype"])
                    #update the values
                    Nmoves = ans["Nmoves"]
                    soltype = ans["soltype"]
                    s = news

    return (soltype, Nmoves, s)


########################################
# Parallel_search algorithm the randomSearch algorithm on several CPUs
#######################################

def parallel_search(Ncpu, Nflips_list, s0):
    # Ncpu is the number of cpu
    # Nflips_list  = [N1, N2, ... ] At run k, all processors perform Nk flips in parallel. 
    # At the end of the k run, all processors "restart" with the best result
    # s0 is the initial puzzle.

    s = s0 
    Nruns = len(Nflips_list)

    curNmoves = 0

    for irun in range(Nruns):
        Nflips = Nflips_list[irun]
        print("\n\n" + "#"*30)
        print("# Run n =", irun+1, " with ", Nflips, "flips")
        print("#"*30)

        if Ncpu == 1 : # for the debug
            list_ans = map( randomSearch, [(s, Nflips)] )

        else: 
            with Pool(Ncpu) as pool:
                list_ans = pool.map( randomSearch, [(s, Nflips) for _ in range(Ncpu)])

        # we take the best solution after all the runs
        #soltype, Nmoves, s = max(list_ans, key=lambda x: [x[1], x[0]])
        soltype, Nmoves, s = max(list_ans, key=lambda x: x[0])

        print("\n" + "_"*30)
        print("| After this run")
        print("| s = ", s)
        print("| soltype = ", soltype)
        print("_"*30)

        if Nmoves > curNmoves: curNmoves = Nmoves
        else: break #no progression at this run, so we can stop the algorithm

    return (solType, Nmoves, s)