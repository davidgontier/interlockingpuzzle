###########################################
# Parameter file

sizePuzzle = (5,4,5) # size of the board.

listPieces = ["1", "2", "3", "4"] # 4 pieces

# the "fixed" string contains: 
# the number i if we want the cell i to be there
# C if we want a full cell at the end (not empty cell)
# X if we want a hole (we cannot add a cell at this location)

fixed = "CCXXC CCXXX CCXXX CCXXC   CCCCC CCCCC CCCCC CCCCC   CCCCC CCCCC CCCCC CCCCC   CCCCC CCXXX CCXXX CCCCC   CCXXX CCXXX CCXXX CCXXX"

# the s0 string is the initial string
# must have at least 2 non-zero number
s0 = "10000 00000 00000 00000   00000 00000 00000 00000   00000 00000 00000 00000   00000 00000 00000 00000   20000 00000 00000 00000   "

# the file to record the results
filename = "record.out"

