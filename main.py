import multiprocessing
import findBestPuzzles as fBP
from parameters import *

import os

def myprint(filename, s):
    print(s)
    with open(filename, "a") as file:
        file.write("\n" + s)

####################################################
## MAIN
###################################################

if __name__ == "__main__":
    # filename is in the parameters file

    Ncpu = multiprocessing.cpu_count() - 1

    myprint(filename, "Number of processes = " + str(Ncpu))

    Nflips_list = [10000, 5000, 5000]
    Nsearchs = 10 #number of times to start the algorithm

    for i in range(Nsearchs):
        myprint(filename, "\n" + "*"*60 )
        myprint(filename, "Search i = " + str(i) )
        soltype, Nmoves, s = fBP.parallel_search(Ncpu, Nflips_list, s0)

        # print only if interesting
        #########################
        if Nmoves > 8 or max(solType) > 5:
            myprint(filename, "s = " + s)
            myprint(filename, "Nmoves = " + str(Nmoves) + " soltype = " + str(soltype) + "\n" )
