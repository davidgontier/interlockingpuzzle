import numpy as np
import itertools
import SolvingBoard as SB

###############################################
def cut(x, S): return min(S-1, max(0, x))

# The directions
Directions = {
    'L': (-1, 0, 0),
    'R': (1, 0, 0),
    'F': (0, -1, 0),
    'B': (0, 1, 0),
    'U': (0, 0, 1),
    'D': (0, 0, -1)
}

########################################
########################################

class Piece:
    def __init__(self, puzzle, numPiece):
        #  puzzle   --> array of size (Sx, Sy, Sz)
        #  numPiece --> the number of the piece to extract

        self.piece_array = puzzle == numPiece
        self.shape = puzzle.shape

        self.list_cells = [] # the list of occupied cells
        self.size = 0 #number of cells
        X,Y,Z = self.shape

        for (x,y,z) in itertools.product( range(X), range(Y), range(Z)):
            if self.piece_array[x,y,z]: 
                self.size += 1
                self.list_cells.append((x,y,z))
    
    def is_connected(self):
        # Check if the piece is connected
        connected_array = np.zeros( self.shape, dtype = int )
        list_to_visit = []
        Sx, Sy, Sz = self.shape
        n = 1

        list_to_visit.append( self.list_cells[0] )

        # while there is something to visit
        while len(list_to_visit) > 0:
            x,y,z = list_to_visit.pop()
            connected_array[x,y,z] = 1
            
            for _, (dx, dy, dz) in Directions.items():
                X, Y, Z = cut(x+dx, Sx), cut(y+dy, Sy), cut(z+dz, Sz)
                if connected_array[X,Y,Z] == 0 and self.piece_array[X,Y,Z]:
                    list_to_visit.append([X,Y,Z])
                    n += 1
                    connected_array[X,Y,Z] = 1
        #
        return n == self.size

    #def is_printable(self): TODO
 

class Puzzle:
    def __init__(self, s, sizePuzzle):
        # sizeBoard is the size of the puzzle
        # s is the initial string
        s = s.replace(' ', '')
        s = s.replace('\n', '')
        self.s = s

        X, Y, Z = sizePuzzle
        self.puzzle = np.zeros(sizePuzzle, dtype=int) # empty board
        for (x,y,z) in itertools.product (range(X), range(Y), range(Z)):
            self.puzzle[x, y, z] = s[x*Y*Z + y*Z + z]

        # create the pieces
        X,Y,Z = self.puzzle.shape
        setPieces = set( self.puzzle.reshape(X*Y*Z) ) # all numbers present
        setPieces.remove(0)# remove 0

        dict_pieces = {}
        for i in setPieces:
            dict_pieces[i] = Piece(self.puzzle, i)
        self.dict_pieces = dict_pieces

    def is_valid(self):
        # check if the pieces are valid
        return all( [P.is_connected() for _,P in self.dict_pieces.items()])
    
    def solve(self):
        # solve the puzzle, see Solving Board
        self.board = SB.Board( self.dict_pieces)
        self.solver = SB.Solver( self )
        return self.solver.solve()
